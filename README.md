# 基于mindspore实现手写汉字识别

## 项目要求

基于MindSpore的实现在线手写汉字识别，主要包括手写汉字检测和手写汉字识别，能较准确的对标准字体的手写文字进行识别，识别后通过人工干预对文本进行适当修正。需要有一定的创新特性，代码达到合入社区标准及规范。



## 项目方案

在自然场景下进行手写汉字识别主要分为两个步骤：手写汉字检测和手写汉字识别。如下图所示，其中，手写汉字检测主要目标是从自然场景的输入图像中寻找到手写汉字区域，并将手写汉字区域从原始图像中分离出来；手写汉字识别的主要目标是从分离出来的图像中准确地识别出该手写汉字的含义。

![image-20210815191837705](README.assets/image-20210815191837705.png)

对于手写汉字检测，考虑采用CTPN算法，CTPN是在ECCV 2016提出的一种文字检测算法。CTPN是在Faster RCNN的基础上结合CNN与LSTM深度网络，能有效的检测出复杂场景的横向分布的文字。对于手写汉字识别考虑使用CNN+RNN+CTC（CRNN+CTC）方法进行识别。



## 项目进度

根据项目申请书中时间安排，目前进度如下表格所示

### 时间安排

| 时间段    | 具体任务                                                     | 完成情况 |
| --------- | ------------------------------------------------------------ | -------- |
| 7.1-7.7   | 梳理CTPN和CRNN+CTC原论文细节和原理                           | ✅        |
| 7.7-7.14  | 搭建mindspore环境，学习其基本使用                            | ✅        |
| 7.14-7.28 | 获取数据集并进行预处理                                       | ✅        |
| 7.28-8.15 | 搭建CTPN网络并进行训练                                       | ✅        |
| 8.15-8.30 | 搭建CRNN+CTC网络，并进行训练                                 | ✅        |
| 8.30-9.15 | 对文字识别结果进行后处理，对误识别的文字进行矫正             | ✅        |
| 9.15-9.23 | 对代码进行适当优化，解决原始CRNN+CTC无法解决竖向文本识别的问题。 | ✅        |
| 9.23-9.30 | 综合考虑各种情况，对文字检测和识别部分代码进行整合，符合规范要求 | ✅        |

目前已经梳理CTPN和CRNN+CTC原论文细节和原理 ，学习Mindspore基本使用，之后对选取CASIA-HWDB数据集进行相应处理来训练。

实现手写汉字识别基本框架，搭建CTPN网络，手写汉字检测基本框架，搭建好CRNN+CTC网络结构，将两部分合并，最终实现端到端的手写汉字识别

## 代码

#### 代码目录说明

```shell
.
└─ctpn
  ├── README.md                             # network readme
  ├── src
  │   ├── CTPN
  │   │   ├── BoundingBoxDecode.py          # bounding box decode
  │   │   ├── BoundingBoxEncode.py          # bounding box encode
  │   │   ├── __init__.py                   # package init file
  │   │   ├── anchor_generator.py           # anchor generator
  │   │   ├── bbox_assign_sample.py         # proposal layer
  │   │   ├── proposal_generator.py         # proposla generator
  │   │   ├── rpn.py                        # region-proposal network
  │   │   └── vgg16.py                      # backbone
  │   ├── config.py                         # training configuration
  │   ├── convert_dataset.py              	# convert dataset label
  │   ├── create_dataset.py                 # create mindrecord dataset
  │   ├── ctpn.py                           # ctpn network definition
  │   ├── dataset.py                        # data proprocessing
  │   ├── lr_schedule.py                    # learning rate scheduler
  │   ├── network_define.py                 # network definition
  │   └── text_connector
  │       ├── __init__.py                   # package init file
  │       ├── connect_text_lines.py         # connect text lines
  │       ├── detector.py                   # detect box
  │       ├── get_successions.py            # get succession proposal
  │       └── utils.py                      # some functions which is commonly used
  └── train.py                              # train net

└─-- crnn_ctc/
    |---train.py    						# Training script
    |---src
        |---__init__.py    					# package init file
        |---cnn_ctc.py    					# crnn_ctc netowrk definition
        |---config.py    					# configuration
        |---callback.py    					# loss callback file
        |---dataset.py    					# Data preprocessing for training and evaluation
        |---util.py    						# some functions which is commonly used
```

