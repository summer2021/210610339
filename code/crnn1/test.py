'''
Date: 2021-09-16 16:14:59
LastEditors: xgy
LastEditTime: 2021-09-16 16:17:23
FilePath: \code\crnn_ctc\test.py
'''

import os
from tqdm import tqdm
import numpy as np
from PIL import Image, ImageFile
from src.config import config1, label_dict


# class Dataset1:
#     """
#     create train or evaluation dataset for crnn
#
#     Args:
#         img_root_dir(str): root path of images
#         max_text_length(int): max number of digits in images.
#         device_target(str): platform of training, support Ascend and GPU.
#     """
#
#     def __init__(self, img_dir, label_dir, config=config1):
#         if not os.path.exists(img_dir):
#             raise RuntimeError("the input image dir {} is invalid!".format(img_dir))
#         if not os.path.exists(label_dir):
#             raise RuntimeError("the label dir of input image {} is invalid!".format(label_dir))
#         self.img_dir = img_dir
#         self.label_dir = label_dir
#         img_files = os.listdir(img_dir)
#         label_files = os.listdir(label_dir)
#
#         self.img_names = {}
#         self.img_list = img_files
#         self.text_length = []
#         self.dict = {}
#
#         self.label_dict = label_dict + '-'
#         for i,char in enumerate(self.label_dict):
#             self.dict[char] = i+1
#
#         for img_file, label_file in zip(img_files, label_files):
#             with open(os.path.join(label_dir, label_file), 'r', encoding='gbk') as f:
#                 label = f.read()
#             self.img_names[img_file] = label
#             self.text_length.append(len(label))
#             # if len(label) > self.max_text_length:
#             #     self.max_text_length = len(label)
#         self.max_text_length = config.max_text_length
#         self.blank = config.blank
#         self.class_num = config.class_num
#         print(f'Finish loading {len(img_files)} images!')
#
#     def __len__(self):
#         return len(self.img_names)
#
#     def __getitem__(self, item):
#         img_name = self.img_list[item]
#         im = Image.open(os.path.join(self.img_dir, img_name))
#         im = im.convert("RGB")
#         r, g, b = im.split()
#         im = Image.merge("RGB", (b, g, r))
#         image = np.array(im)
#         label_str = self.img_names[img_name]
#         label = []
#         for c in label_str:
#             if c in label_dict:
#                 label.append(self.dict[c])
#         label.extend([0] * (self.max_text_length - len(label)))
#         label = np.array(label)
#         return image, label

class Dataset1:
    """
    create train or evaluation dataset for crnn

    Args:
        img_root_dir(str): root path of images
        max_text_length(int): max number of digits in images.
        device_target(str): platform of training, support Ascend and GPU.
    """

    def __init__(self, img_dir, label_dir, config=config1):
        if not os.path.exists(img_dir):
            raise RuntimeError("the input image dir {} is invalid!".format(img_dir))
        if not os.path.exists(label_dir):
            raise RuntimeError("the label dir of input image {} is invalid!".format(label_dir))
        self.img_dir = img_dir
        self.label_dir = label_dir
        img_files = os.listdir(img_dir)
        label_files = os.listdir(label_dir)

        self.img_names = {}
        self.img_list = img_files
        self.text_length = []
        # self.dict = {}

        # self.label_dict = label_dict + '-'
        # for i,char in enumerate(self.label_dict):
        #     self.dict[char] = i+1

        for img_file, label_file in zip(img_files, label_files):
            with open(os.path.join(label_dir, label_file), 'r', encoding='gbk') as f:
                label = f.read()
            self.img_names[img_file] = label
            self.text_length.append(len(label))
            # if len(label) > self.max_text_length:
            #     self.max_text_length = len(label)
        self.max_text_length = config.max_text_length
        self.blank = config.blank
        self.class_num = config.class_num
        print(f'Finish loading {len(img_files)} images!')

    def __len__(self):
        return len(self.img_names)

    def __getitem__(self, item):
        img_name = self.img_list[item]
        im = Image.open(os.path.join(self.img_dir, img_name))
        im = im.convert("RGB")
        r, g, b = im.split()
        im = Image.merge("RGB", (b, g, r))
        image = np.array(im)
        label_str = self.img_names[img_name]
        label = []
        for c in label_str:
            if c in label_dict:
                label.append(label_dict.index(c))
        label.extend([int(config1.blank)] * (self.max_text_length - len(label)))
        label = np.array(label)
        return image, label

img_dir = r'E:\program_lab\python\dataset\CASIA\textline\recognition\train_dataset\images'
label_dir = r'E:\program_lab\python\dataset\CASIA\textline\recognition\train_dataset\labels'
a = Dataset1(img_dir,label_dir)
print(a[0])
img_dir = r'E:\program_lab\python\dataset\CASIA\textline\recognition\test_dataset\images'
label_dir = r'E:\program_lab\python\dataset\CASIA\textline\recognition\test_dataset\labels'
b = Dataset1(img_dir,label_dir)

with open('char_std_5990.txt', 'rb') as file:
	char_dict = {num : char.strip().decode('gbk','ignore') for num, char in enumerate(file.readlines())}
