"""character recognition """
import os
import argparse
import time
import numpy as np
from mindspore import context
from mindspore.train.model import Model
from mindspore.train.serialization import load_checkpoint, load_param_into_net
from mindspore.common import set_seed

from ctpn.src.ctpn import CTPN
from ctpn.src.config import config
from ctpn.src.dataset import create_ctpn_dataset
from ctpn.src.text_connector.detector import detect

from crnn.src.loss import CTCLoss
from crnn.src.dataset import create_dataset
from crnn.src.crnn_ctc import crnn
from crnn.src.metric import CRNNAccuracy
from crnn.src.config import config1 as config

set_seed(1)

parser = argparse.ArgumentParser(description="Character recognition")
parser.add_argument("--dataset_path", type=str, default="", help="Dataset path.")
parser.add_argument("--image_path", type=str, default="", help="Image path.")
parser.add_argument("--ctpn_checkpoint", type=str, default="", help="Checkpoint file path of CTPN.")
parser.add_argument("--crnn_checkpoint", type=str, default="", help="Checkpoint file path of CRNN.")
parser.add_argument("--device_id", type=int, default=0, help="Device id, default is 0.")
args_opt = parser.parse_args()
context.set_context(mode=context.GRAPH_MODE, device_target="Ascend", device_id=args_opt.device_id)

def ctpn_infer_test(dataset_path='', ckpt_path='', img_dir=''):
    """ctpn infer."""
    print("ckpt path of ctpn is {}".format(ckpt_path))
    ds = create_ctpn_dataset(dataset_path, batch_size=config.test_batch_size, repeat_num=1, is_training=False)
    config.batch_size = config.test_batch_size
    total = ds.get_dataset_size()
    print("*************total dataset size is {}".format(total))
    net = CTPN(config, is_training=False)
    param_dict = load_checkpoint(ckpt_path)
    load_param_into_net(net, param_dict)
    net.set_train(False)
    eval_iter = 0

    print("\n========================================\n")
    print("Processing, please wait a moment.")
    img_basenames = []
    output_dir = os.path.join(os.getcwd(), "submit")
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    for file in os.listdir(img_dir):
        img_basenames.append(os.path.basename(file))
    img_basenames = sorted(img_basenames)
    for data in ds.create_dict_iterator():
        img_data = data['image']
        img_metas = data['image_shape']
        gt_bboxes = data['box']
        gt_labels = data['label']
        gt_num = data['valid_num']

        start = time.time()
        # run net
        output = net(img_data, gt_bboxes, gt_labels, gt_num)
        gt_bboxes = gt_bboxes.asnumpy()
        gt_labels = gt_labels.asnumpy()
        gt_num = gt_num.asnumpy().astype(bool)
        end = time.time()
        proposal = output[0]
        proposal_mask = output[1]
        print("start to draw pic")
        for j in range(config.test_batch_size):
            img = img_basenames[config.test_batch_size * eval_iter + j]
            all_box_tmp = proposal[j].asnumpy()
            all_mask_tmp = np.expand_dims(proposal_mask[j].asnumpy(), axis=1)
            using_boxes_mask = all_box_tmp * all_mask_tmp
            textsegs = using_boxes_mask[:, 0:4].astype(np.float32)
            scores = using_boxes_mask[:, 4].astype(np.float32)
            shape = img_metas.asnumpy()[0][:2].astype(np.int32)
            bboxes = detect(textsegs, scores[:, np.newaxis], shape)
            from PIL import Image, ImageDraw
            im = Image.open(img_dir + '/' + img)
            draw = ImageDraw.Draw(im)
            image_h = img_metas.asnumpy()[j][2]
            image_w = img_metas.asnumpy()[j][3]
            gt_boxs = gt_bboxes[j][gt_num[j], :]
            for gt_box in gt_boxs:
                gt_x1 = gt_box[0] / image_w
                gt_y1 = gt_box[1] / image_h
                gt_x2 = gt_box[2] / image_w
                gt_y2 = gt_box[3] / image_h
                draw.line([(gt_x1, gt_y1), (gt_x1, gt_y2), (gt_x2, gt_y2), (gt_x2, gt_y1), (gt_x1, gt_y1)],\
                    fill='green', width=2)
            file_name = "res_" + img.replace("jpg", "txt")
            output_file = os.path.join(output_dir, file_name)
            f = open(output_file, 'w')
            for bbox in bboxes:
                x1 = bbox[0] / image_w
                y1 = bbox[1] / image_h
                x2 = bbox[2] / image_w
                y2 = bbox[3] / image_h
                draw.line([(x1, y1), (x1, y2), (x2, y2), (x2, y1), (x1, y1)], fill='red', width=2)
                str_tmp = str(int(x1)) + "," + str(int(y1)) + "," + str(int(x2)) + "," + str(int(y2))
                f.write(str_tmp)
                f.write("\n")
            f.close()
            im.save(img)

def crnn_infer_test(config,args_opt):
    config.batch_size = 1
    max_text_length = config.max_text_length
    input_size = config.input_size
    # create dataset
    dataset = create_dataset(img_dir=os.path.join(args_opt.dataset_path,'images'), 
                             label_dir=os.path.join(args_opt.dataset_path,'labels'), 
                             batch_size=config.batch_size,
                             is_training=False,
                             config=config)
    step_size = dataset.get_dataset_size()
    loss = CTCLoss(max_sequence_length=config.num_step,
                   max_label_length=max_text_length,
                   batch_size=config.batch_size)
    net = crnn(config)
    # load checkpoint
    param_dict = load_checkpoint(args_opt.checkpoint_path)
    load_param_into_net(net, param_dict)
    net.set_train(False)
    # define model
    model = Model(net, loss_fn=loss, metrics={'CRNNAccuracy': CRNNAccuracy(config)})
    # start evaluation
    res = model.eval(dataset, dataset_sink_mode=args_opt.platform == 'Ascend')
    print("result:", res, flush=True)


if __name__ == '__main__':
    ctpn_infer_test(args_opt.dataset_path, args_opt.ctpn_checkpoint, img_dir=args_opt.image_path)
    crnn_infer_test(config,args_opt)